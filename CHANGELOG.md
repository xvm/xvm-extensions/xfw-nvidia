# Changelog

## v0.2.0

* fixed compatbility with WoT 1.18.0

## v0.1.8

* retire D3DHook in favor of Kiero

## v0.1.7

* fix incompatibility fir IzeBerg's OffHangar mod

## v0.1.6

* AMD64 fixes
* stability fixes

## v0.1.5

* initial AMD64 implementation

## v0.1.3

* transition to pybind11
* stability fixes

## v0.1.2

* fixed compatibility with WoT 1.6.0

## v0.1.1

* stability fixes

## v0.1.0

- update AnselSDK to 1.6.490
- disable Ansel on login screen
- disable Ansel for non-arcade cameras (fixed black screen in battle)

## v0.0.9

- switch to XFW.Native/d3dhook for DXGI hooking

## v0.0.8

- get rid of custom WoT loader

## v0.0.7

- code was published under LGPLv3 license

## v0.0.6

- added 360-stereo capture
- added `ansel_session_start()`, `ansel_session_stop()` and `ansel_session_isrunning()` python functions

## v0.0.5

- implemented replay autopause on entering in Ansel
- added replay speed slider

## v0.0.4

- first public version
- fixed FoV slider in replays
- fixed camera movement in replays when the recorded camera is playing
- fixed UI hiding

## v0.0.3

- implemented camera movement and rotation

## v0.0.2

- implemented FoV change

## v0.0.1

- first version
