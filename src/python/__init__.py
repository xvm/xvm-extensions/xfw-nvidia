"""
This file is part of the XFW.NVIDIA project.

Copyright (c) 2018-2022 XVM Team.

XFW.NVIDIA is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XFW.NVIDIA is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

#
# Imports
#

#cpython
import logging

#wot client
from helpers import dependency
from skeletons.connection_mgr import IConnectionManager

#xfw.loader
from xfw_native.python import XFWNativeModuleWrapper



#
# Globals
#

g_xfwnvidia = None
g_xfwnvidia_ansel = None
g_xfwnvidia_ansel_inited = False



#
# Private
#

def __on_disconnected():
    try:
        if g_xfwnvidia_ansel is not None:
            g_xfwnvidia_ansel.ansel_session_stop()
    except Exception:
        logging.getLogger('XFW/NVIDIA').exception("on_disconnected:")



#
# XFW Loader
#

def xfw_is_module_loaded():
    global g_xfwnvidia_ansel_inited
    return g_xfwnvidia_ansel_inited

def xfw_module_init():
    global g_xfwnvidia
    global g_xfwnvidia_ansel
    global g_xfwnvidia_ansel_inited
    
    g_xfwnvidia = XFWNativeModuleWrapper('com.modxvm.xfw.nvidia', 'xfw_nvidia.pyd', 'XFW_NVIDIA')
    g_xfwnvidia_ansel = g_xfwnvidia.Ansel()
    
    g_xfwnvidia_ansel_inited = g_xfwnvidia_ansel.init()
    if g_xfwnvidia_ansel_inited:
        connection_manager = dependency.instance(IConnectionManager)
        connection_manager.onDisconnected += __on_disconnected
    else:
        logging.getLogger('XFW/NVIDIA').error("init: failed to initialize NVIDIA Ansel. Possible reasons: you are using non-NVIDIA GPU or NVIDIA Experience is not installed or NVIDIA overlay is disabled")
