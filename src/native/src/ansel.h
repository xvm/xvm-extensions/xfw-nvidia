/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2022 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

//Windows SDK
#include <dxgi.h>

//Pybind11
#include <pybind11/pybind11.h>

//Nvidia Ansel
#include <AnselSDK.h>

//XFW Hooks
#include <xfw_native_hooks_d3d.h>

//XFW NVIDIA
#include "dllmain.h"
#include "utils.h"
#include "wot_avatar.h"
#include "wot_camera.h"
#include "wot_replay.h"
#include "wot_ui.h"

class Ansel {
public:
    Ansel();
    ~Ansel();

    bool Init();
    void Deinit();

    void StartSession();
    void StopSession();

    [[nodiscard]] bool IsRunning() const;

private:
    
    //DXGI
    bool dxgi_hook();
    bool dxgi_unhook();
    void onPresent(IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags);;

    //Ansel
    static ansel::StartSessionStatus CallBack_StartSession(ansel::SessionConfiguration& settings, void* userPointer);
    static void CallBack_StopSession(void* userPointer);
    static void CallBack_StartCapture(const ansel::CaptureConfiguration& settings, void* userPointer);
    static void CallBack_StopCapture(void* userPointer);
    static void CreatePlaybackSpeedControl(uint32_t control_id, void* userData);
    static void CallBack_PlaybackControl(const ansel::UserControlInfo& info);

private:
    XFW::Native::Hooks::HookmanagerD3D& _hookmgr;
    ansel::Configuration config{};
    WotCamera _wot_camera;
    WotReplay _wot_replay;

    bool _isSessionEnabled = false;
};
