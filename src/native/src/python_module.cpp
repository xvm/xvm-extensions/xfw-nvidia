/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2022 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <pybind11/pybind11.h>

#include "ansel.h"
#include "wot_ui.h"


class ProjectionAccess
{
public:
    void rampFov() {};

    float farPlane = 0.0f;
    float fov = 0.0f;
    float nearPlane = 0.0f;
};


PYBIND11_MODULE(XFW_NVIDIA, m) {
    m.doc() = "XFW NVIDIA module";
    m.def("ui_switch_visibility", &WotUi::SwitchVisibility);

    pybind11::class_<ProjectionAccess>(m, "ProjectionAccess")
        .def(pybind11::init<>())
        .def("rampFov", &ProjectionAccess::rampFov)
        .def_readwrite("farPlane", &ProjectionAccess::farPlane)
        .def_readwrite("fov", &ProjectionAccess::fov)
        .def_readwrite("nearPlane", &ProjectionAccess::nearPlane);

    pybind11::class_<Ansel>(m, "Ansel")
        .def(pybind11::init<>())
        .def("init", &Ansel::Init, "Init NVidia Ansel")
        .def("session_is_running", &Ansel::IsRunning, "Get NVidia Ansel session status")
        .def("session_start", &Ansel::StartSession, "Start NVidia Ansel session")
        .def("session_stop", &Ansel::StopSession, "Stop NVidia Ansel session");
}
