/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2022 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

//XFW NVIDIA
#include "ansel.h"

using namespace std::placeholders;
using namespace XFW::Native::Hooks;


//
// Init
//

Ansel::Ansel() : _hookmgr(XFW::Native::Hooks::HookmanagerD3D::instance()) {

}

Ansel::~Ansel()
{
    Deinit();
}

bool Ansel::Init()
{
    if (dxgi_hook()) {
        return false;
    }

    if (!ansel::isAnselAvailable()) {
        return false;
    }

    config.captureLatency = 1;
    config.captureSettleLatency = 10;

    config.right = { 1.0f, 0.0f, 0.0f };
    config.up = { 0.0f, 1.0f, 0.0f };
    config.forward = { 0.0f, 0.0f, 1.0f };

    config.fovType = ansel::FovType::kVerticalFov;
    config.gameWindowHandle = Utils::GetLargestWindow();

    config.isCameraFovSupported = true;
    config.isCameraRotationSupported = true;
    config.isCameraTranslationSupported = true;

    config.isCameraOffcenteredProjectionSupported = false;

    config.metersInWorldUnit = 1.0f;
    config.rotationalSpeedInDegreesPerSecond = 45.0f;
    config.translationalSpeedInWorldUnitsPerSecond = 7.0f;

    config.titleNameUtf8 = "WorldOfTanks";
    config.userPointer = this;

    config.startSessionCallback = CallBack_StartSession;
    config.stopSessionCallback = CallBack_StopSession;

    config.startCaptureCallback = CallBack_StartCapture;
    config.stopCaptureCallback = CallBack_StopCapture;

    ansel::setConfiguration(config);

    return true;
}

void Ansel::Deinit()
{
    ansel::stopSession();
    dxgi_unhook();
}


//
// Public
//

void Ansel::StartSession()
{
    ansel::startSession();
}

void Ansel::StopSession()
{
    ansel::stopSession();
}

bool Ansel::IsRunning() const
{
    return _isSessionEnabled;
}



//
// Private
//

bool Ansel::dxgi_hook() {
    return _hookmgr.IDXGISwapChain_Present_register("xfw_nvidia", HookPlace::Before, std::bind(&Ansel::onPresent, this, _1, _2, _3));
}

bool Ansel::dxgi_unhook() {
    return _hookmgr.IDXGISwapChain_Present_unregister("xfw_nvidia", HookPlace::Before);
}



//
// Callbacks DXGI
//

void Ansel::onPresent(IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags)
{
    if (_isSessionEnabled) {
        pybind11::gil_scoped_acquire guard;
        auto camera = _wot_camera.GetCameraSettings();
        ansel::updateCamera(camera);
        _wot_camera.SetCameraSettings(camera);
    }
}



//
// Callbacks Ansel
//

ansel::StartSessionStatus Ansel::CallBack_StartSession(ansel::SessionConfiguration& settings, void* userPointer)
{
    pybind11::gil_scoped_acquire guard;

    //Ansel settings
    settings.isFovChangeAllowed = true;
    settings.maximumFovInDegrees = 140.0f; //after 149 game works incorrectly
    settings.isRawAllowed = false; //game does not supports HDR?
    settings.isRotationAllowed = true;
    settings.isTranslationAllowed = true;
    settings.is360MonoAllowed = true;
    settings.is360StereoAllowed = true;
    settings.isHighresAllowed = false;
    settings.isPauseAllowed = true;

    if (WotAvatar::GetPlayer().is_none()) {
        return ansel::StartSessionStatus::kDisallowed;
    }

    if (!WotAvatar::GetPlayerAvatar().is_none()) {

        if (WotAvatar::GetAvatarControllerName() != "arcade") {
            return ansel::StartSessionStatus::kDisallowed;
        }

        WotAvatar::EnableAvatarCurrentController(false);
    }

    auto* ansel = reinterpret_cast<Ansel*>(userPointer);

    WotUi::SwitchVisibility(false);

    ansel->_wot_camera.Enable();

    if (WotReplay::IsReplayPlaying())
    {
        ansel->_wot_replay.Enable();
        WotReplay::SetReplaySpeedIndex(0);
        Ansel::CreatePlaybackSpeedControl(0, userPointer);
    }

    ansel->_isSessionEnabled = true;
    return ansel::StartSessionStatus::kAllowed;
}

void Ansel::CallBack_StopSession(void* userPointer)
{
    pybind11::gil_scoped_acquire guard;

    auto* ansel = reinterpret_cast<Ansel*>(userPointer);

    //restore camera
    ansel->_wot_camera.Disable();

    //restore replay settings
    ansel->_wot_replay.Disable();

    //enable avatar controller
    WotAvatar::EnableAvatarCurrentController(true);

    //remove ansel control
    ansel::removeUserControl(0);

    //show WoT UI
    WotUi::SwitchVisibility(true);

    ansel->_isSessionEnabled = false;
}


void Ansel::CallBack_StartCapture(const ansel::CaptureConfiguration& settings, void* userPointer)
{
}


void Ansel::CallBack_StopCapture(void* userPointer)
{
}


void Ansel::CreatePlaybackSpeedControl(uint32_t control_id, void* userData)
{
    const float default_value = 0.0f;

    ansel::UserControlDesc controlDesc;

    controlDesc.labelUtf8 = "Playback speed";

    controlDesc.info.userControlId = control_id;
    controlDesc.info.userControlType = ansel::UserControlType::kUserControlSlider;
    controlDesc.info.userPointer = userData;

    controlDesc.info.value = &default_value;

    controlDesc.callback = Ansel::CallBack_PlaybackControl;
    ansel::addUserControl(controlDesc);
}


void Ansel::CallBack_PlaybackControl(const ansel::UserControlInfo & info)
{
    auto* ansel = reinterpret_cast<Ansel*>(info.userPointer);
    auto result = (int)std::round(ansel->_wot_replay.GetMaxReplaySpeed() * (*(float*)info.value));
    WotReplay::SetReplaySpeedIndex(result);
}
